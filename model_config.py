# -*- coding: utf-8 -*-

from utils.metadata import get_metadata

metadata = get_metadata()
config = {
    'img_rows': 224,
    'img_cols': 224,
    'channel': 3,
    'num_classes': 2,
    'batch_size': 16,
    'nb_epoch': 50,
    'val_split': 0.8,
    'learning_rate': 10e-3,
    'train_data_dir': './data/train',
    'valid_data_dir': './data/validation',
    'nb_train_samples': metadata['train']['total'],
    'nb_valid_samples': metadata['valid']['total'],
    'class_weights': metadata['class_weights']
}

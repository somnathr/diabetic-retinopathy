# -*- coding: utf-8 -*-

import matplotlib
matplotlib.use('Agg')
import numpy as np
import ast
import scipy
import matplotlib.pyplot as plt
import cv2
import os
import shutil
from glob import glob
from random import sample
from model_config import config as cf
from utils.model_definition import ret_model
from keras.preprocessing import image
from keras.optimizers import Adam
from keras.models import Model


def pretrained_path_to_tensor(img_path):
    # loads RGB image as PIL.Image.Image type
    img = image.load_img(img_path, target_size=(224, 224))
    # convert PIL.Image.Image type to 3D tensor with shape (224, 224, 3)
    x = image.img_to_array(img)
    # convert 3D tensor to 4D tensor with shape (1, 224, 224, 3) and return 4D tensor
    x = np.expand_dims(x, axis=0)
    # convert RGB -> BGR, subtract mean ImageNet pixel, and return 4D tensor
    return x


def get_ResNet():
    # Load our model
    model = ret_model(
        cf['img_rows'],
        cf['img_cols'],
        cf['channel'],
        cf['num_classes'],
        optimizer=Adam(cf['learning_rate']),
        loss='categorical_crossentropy',
        weights_path='./models/weights.hdf5')
    # get AMP layer weights
    all_amp_layer_weights = model.layers[-1].get_weights()[0]
    # extract wanted output
    RetNet_model = Model(
        inputs=model.input,
        outputs=(model.layers[-3].output, model.layers[-1].output))
    return RetNet_model, all_amp_layer_weights


def ResNet_CAM(img_path, model, all_amp_layer_weights):
    # get filtered images from convolutional output + model prediction vector
    last_conv_output, pred_vec = model.predict(
        pretrained_path_to_tensor(img_path))
    # change dimensions of last convolutional outpu tto 7 x 7 x 2048
    last_conv_output = np.squeeze(last_conv_output)
    # get model's prediction (number between 0 and 999, inclusive)
    pred = np.argmax(pred_vec)
    # bilinear upsampling to resize each filtered image to size of original image
    mat_for_mult = scipy.ndimage.zoom(
        last_conv_output, (32, 32, 1), order=1)  # dim: 224 x 224 x 2048
    # get last conv layer size
    width_conv_layer = mat_for_mult.shape[0]
    # get AMP layer weights
    amp_layer_weights = all_amp_layer_weights[:, pred]  # dim: (2048,)
    # get class activation map for object class that is predicted to be in the image
    final_output = np.dot(
        mat_for_mult.reshape((width_conv_layer * width_conv_layer, 1024)),
        amp_layer_weights).reshape(width_conv_layer,
                                   width_conv_layer)  # dim: 224 x 224
    # return class activation map
    return final_output, pred, width_conv_layer


def plot_ResNet_CAM(img_path, dest_path, model, all_amp_layer_weights):
    fig, ax = plt.subplots()
    # get class activation map
    CAM, pred, layer_width = ResNet_CAM(img_path, model, all_amp_layer_weights)
    # load image, convert BGR --> RGB, resize image to 224 x 224,
    im = cv2.resize(
        cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB),
        (layer_width, layer_width))
    # plot image
    ax.imshow(im, alpha=0.6)
    # plot class activation map
    cax = ax.imshow(CAM, cmap='jet', alpha=0.4)
    plt.colorbar(cax)
    # load the dictionary that identifies each ImageNet category to an index in the prediction vector
    with open('dr2_clsid_to_human.txt') as imagenet_classes_file:
        imagenet_classes_dict = ast.literal_eval(imagenet_classes_file.read())
    # obtain the predicted ImageNet category
    ax.set_title(imagenet_classes_dict[pred])
    plt.savefig(dest_path)
    print 'Plotted : ', dest_path


def load_testset(data_dir, sample_size):
    dir_path = os.path.join(data_dir, 'validation')
    val_classes = os.listdir(dir_path)

    # define train and validation dir
    result_dir = os.path.join(DATA_DIR, 'result')

    if not os.path.exists(DATA_DIR):
        os.makedirs(DATA_DIR)
    if os.path.exists(result_dir):
        shutil.rmtree(result_dir)

    # create the folder structures
    for label in val_classes:
        os.makedirs(os.path.join(result_dir, str(label)))

    # store images
    X_img_path = []
    # store labels
    Y_img_dest = []

    for val_class in val_classes:
        # get all images in the class
        list_images = glob(os.path.join(dir_path, val_class) + '/*.jpeg')
        list_images = sample(list_images, sample_size)

        for img in list_images:
            img_name = os.path.basename(img)
            dest_path = os.path.join(result_dir, val_class, img_name)
            Y_img_dest.append(dest_path)

        # create list
        X_img_path.extend(list_images)

    # create arrays
    plot_imgs = dict(zip(X_img_path, Y_img_dest))

    return plot_imgs


if __name__ == '__main__':
    DATA_DIR = './data'

    # load test set
    plot_imgs = load_testset(DATA_DIR, 10)

    ResNet_model, all_amp_layer_weights = get_ResNet()
    for img_path, dest_path in plot_imgs.items():
        plot_ResNet_CAM(img_path, dest_path, ResNet_model,
                        all_amp_layer_weights)

# -*- coding: utf-8 -*-

import pandas as pd
from skimage import io
from skimage.transform import rotate
import cv2
import time


def rotate_images(file_path, degrees_of_rotation, lst_imgs):
    '''
    Rotates image based on a specified amount of degrees

    INPUT
        file_path: file path to the folder containing images.
        degrees_of_rotation: Integer, specifying degrees to rotate the
        image. Set number from 1 to 360.
        lst_imgs: list of image strings.

    OUTPUT
        Images rotated by the degrees of rotation specififed.
    '''

    bak_lst = []
    for l in lst_imgs:
        try:
            img = io.imread(file_path + str(l) + '.jpeg')
            img = rotate(img, degrees_of_rotation)
            io.imsave(
                file_path + str(l) + '_' + str(degrees_of_rotation) + '.jpeg',
                img)
            bak_lst.append(str(l) + '_' + str(degrees_of_rotation))
        except Exception as e:
            print("Skipping" + str(l) + " " + str(e))
            pass
    return bak_lst


def mirror_images(file_path, mirror_direction, lst_imgs):
    '''
    Mirrors image left or right, based on criteria specified.

    INPUT
        file_path: file path to the folder containing images.
        mirror_direction: criteria for mirroring left or right.
        lst_imgs: list of image strings.

    OUTPUT
        Images mirrored left or right.
    '''

    bak_lst = []
    for l in lst_imgs:
        try:
            img = cv2.imread(file_path + str(l) + '.jpeg')
            img = cv2.flip(img, 1)
            cv2.imwrite(file_path + str(l) + '_mir' + '.jpeg', img)
            bak_lst.append(str(l) + '_mir')
        except Exception as e:
            print("Skipping" + str(l) + " " + str(e))
            pass
    return bak_lst


if __name__ == '__main__':
    start_time = time.time()
    trainLabels = pd.read_csv("./data/trainLabels.csv")
    trainLabels.loc[trainLabels['level'] >= 1, 'level'] = 1

    trainLabels['image'] = trainLabels['image'].str.rstrip('.jpeg')
    trainLabels_no_DR = trainLabels[trainLabels['level'] == 0]
    trainLabels_DR = {}
    for i in range(1, trainLabels['level'].max() + 1):
        trainLabels_DR[i] = trainLabels[trainLabels['level'] == i]

    lst_imgs_no_DR = trainLabels_no_DR['image'].tolist()
    lst_imgs_DR = {}
    for i in range(1, trainLabels['level'].max() + 1):
        lst_imgs_DR[i] = trainLabels_DR[i]['image'].tolist()

    print("Starting images")
    print("No_DR " + str(len(lst_imgs_no_DR)))

    # Mirror Images with no DR one time
    print("Mirroring Non-DR Images")
    lst_imgs_no_DR.extend(mirror_images('./data/train_np/', 1, lst_imgs_no_DR))
    print("Augumented No_DR " + str(len(lst_imgs_no_DR)))

    # Rotate all images that have any level of DR
    for i in range(1, trainLabels['level'].max() + 1):
        temp_list = []
        print("Starting images")
        print("DR_" + str(i) + " " + str(len(lst_imgs_DR[i])))

        print("Rotating 90 Degrees, Class : " + str(i))
        temp_list.extend(rotate_images('./data/train_np/', 90, lst_imgs_DR[i]))

        print("Rotating 120 Degrees, Class : " + str(i))
        temp_list.extend(
            rotate_images('./data/train_np/', 120, lst_imgs_DR[i]))

        print("Rotating 180 Degrees, Class : " + str(i))
        temp_list.extend(
            rotate_images('./data/train_np/', 180, lst_imgs_DR[i]))

        print("Rotating 270 Degrees, Class : " + str(i))
        temp_list.extend(
            rotate_images('./data/train_np/', 270, lst_imgs_DR[i]))

        print("Mirroring DR Images, Class : " + str(i))
        temp_list.extend(mirror_images('./data/train_np/', 0, lst_imgs_DR[i]))

        lst_imgs_DR[i].extend(temp_list)
        print("Augumented DR_" + str(i) + " " + str(len(lst_imgs_DR[i])))

    # Generate new Training Labels from the agumented data
    trainLabels_dict = {}
    trainLabels_dict.update(
        dict(zip(lst_imgs_no_DR, [0] * len(lst_imgs_no_DR))))
    for i in range(1, trainLabels['level'].max() + 1):
        trainLabels_dict.update(
            dict(zip(lst_imgs_DR[i], [i] * len(lst_imgs_DR[i]))))

    # Create Pandas table
    print("Label Count: " + str(len(trainLabels_dict)))
    trainLabels = pd.DataFrame(
        trainLabels_dict.items(), columns=['image', 'level'])

    # Write to CSV
    print("Writing DataFrame of size: " + str(trainLabels.shape))
    trainLabels.to_csv(
        './data/trainLabels_master.csv', index=False, header=True)

    print("Completed")
    print("--- %s seconds ---" % (time.time() - start_time))

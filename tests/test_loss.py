import tensorflow as tf
from utils.loss import quadratic_weighted_kappa
from quadratic_weighted_kappa_np import quadratic_weighted_kappa_np
from keras.utils import to_categorical
import numpy as np


def run_kappa_loss():
    a = np.array([
        0, 0, 0, 1, 2, 4, 4, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0,
        0, 0, 0, 1, 0, 2
    ])
    b = np.array([
        0, 0, 0, 0, 2, 0, 4, 0, 0, 0, 0, 2, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 0, 0
    ])
    c = np.array([
        1, 1, 1, 0, 1, 0, 2, 1, 0, 1, 1, 2, 1, 1, 1, 2, 2, 1, 2, 3, 4, 0, 0, 2,
        3, 4, 1, 3, 2, 0
    ])

    x = tf.placeholder(tf.int32, shape=(None, 5))
    y = tf.placeholder(tf.int32, shape=(None, 5))
    z = tf.placeholder(tf.int32, shape=(None, 5))
    loss_tf1 = quadratic_weighted_kappa(x, y)
    loss_tf2 = quadratic_weighted_kappa(x, x)
    loss_tf3 = quadratic_weighted_kappa(x, z)

    a_ex = to_categorical(a, 5)
    b_ex = to_categorical(b, 5)
    c_ex = to_categorical(c, 5)

    l1np = quadratic_weighted_kappa_np(a, b, 0, 4)
    l2np = quadratic_weighted_kappa_np(a, a, 0, 4)
    l3np = quadratic_weighted_kappa_np(a, c, 0, 4)

    with tf.Session() as sess:
        l1, l2, l3 = sess.run([loss_tf1, loss_tf2, loss_tf3], {
            x: a_ex,
            y: b_ex,
            z: c_ex
        })
        print 'Checking cross loss'
        print round(l1, 6) == round(l1np, 6), 'Numpy', l1np, 'TF', l1
        print 'Checking self loss'
        print round(l2, 6) == round(l2np, 6), 'Numpy', l2np, 'TF', l2
        print 'Checking Diff loss'
        print round(l3, 6) == round(l3np, 6), 'Numpy', l3np, 'TF', l3

# Download datasets
BASEDIR=$(dirname "$0")
mkdir $BASEDIR/data/
DATADIR=$BASEDIR/data/

# Download training set
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1oZxpHtdUgQ82FQDovc2COA9jGjJAi_ZK' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1oZxpHtdUgQ82FQDovc2COA9jGjJAi_ZK" -nc -P $DATADIR -c -O train.zip && rm -rf /tmp/cookies.txt

# Download test set
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1BvXfe7XH-GWqBtJg_CvBbWPy_gaCMna9' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1BvXfe7XH-GWqBtJg_CvBbWPy_gaCMna9" -nc -P $DATADIR -c -O test.zip && rm -rf /tmp/cookies.txt

# Download training labels
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1WhTeKfL7m_LIus6bcjCtc8imPvsoI9kh' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1WhTeKfL7m_LIus6bcjCtc8imPvsoI9kh" -nc -P $DATADIR -c -O trainLabels.zip && rm -rf /tmp/cookies.txt

# Make models directory
mkdir $BASEDIR/models/
MODELSDIR=$BASEDIR/models/

# Download Resnet weights
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=15OeJohgTAVXBzlw3-secVqtw1YWpep19' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=15OeJohgTAVXBzlw3-secVqtw1YWpep19" -nc -P $MODELSDIR -c -O resnet50_weights_tf_dim_ordering_tf_kernels.h5 && rm -rf /tmp/cookies.txt

# -*- coding: utf-8 -*-

import zipfile
import os
import Image
import sys
import pandas as pd
import shutil
from random import shuffle
import json
'''
Usage:

python /path/to/file path/to/zip path/to/label/file validation_split
expand train dir to train and validation folder
folder stucture is as follows

data/
    train/
        class-1/
        class-2/
        ...
        ...
    validation/
        class-1/
        class-2/
        ...
        ...
'''

DATA_DIR = './data'

if __name__ == '__main__':
    FILE_PATH = sys.argv[1]
    LABEL_PATH = sys.argv[2]
    VALID_SPLIT = float(sys.argv[3])
    archive = zipfile.ZipFile(FILE_PATH, 'r')
    labels = pd.read_csv(LABEL_PATH)

    # define train and validation dir
    train_dir = os.path.join(DATA_DIR, 'train')
    valid_dir = os.path.join(DATA_DIR, 'validation')

    if not os.path.exists(DATA_DIR):
        os.makedirs(DATA_DIR)
    if os.path.exists(train_dir):
        shutil.rmtree(train_dir)
    if os.path.exists(valid_dir):
        shutil.rmtree(valid_dir)

    # create the folder structures
    class_names = list(labels['level'].unique())
    for label in class_names:
        os.makedirs(os.path.join(train_dir, str(label)))
        os.makedirs(os.path.join(valid_dir, str(label)))

    # create a image name to label map for faster search
    label_map = pd.Series(
        labels['level'].values, index=labels['image'].values).to_dict()

    # get list of images in the archive
    # first entry is the folder name
    image_list = archive.namelist()[1:]
    shuffle(image_list)
    split_idx = int(len(image_list) * VALID_SPLIT)

    # collect staistics
    train_stat = dict(zip(class_names, [0] * len(class_names)))
    valid_stat = dict(zip(class_names, [0] * len(class_names)))

    # write images to train dir
    for image in image_list[:split_idx]:
        image_name_ext = os.path.basename(image)
        image_name = os.path.splitext(image_name_ext)[0]

        try:
            image_label = label_map[image_name]
            img = Image.open(archive.open(image))
            print 'Extracted', image_name_ext, 'Writing to', image_label
            img.save(os.path.join(train_dir, str(image_label), image_name_ext))
            train_stat[image_label] += 1
        except IOError, e:
            # skip error images
            print 'Skiping', image_name_ext, 'Target', image_label

    # write images to valid dir
    for image in image_list[split_idx:]:
        image_name_ext = os.path.basename(image)
        image_name = os.path.splitext(image_name_ext)[0]

        try:
            image_label = label_map[image_name]
            img = Image.open(archive.open(image))
            print 'Extracted', image_name_ext, 'Writing to', image_label
            img.save(os.path.join(valid_dir, str(image_label), image_name_ext))
            valid_stat[image_label] += 1
        except IOError, e:
            # skip error images
            print 'Skiping', image_name_ext, 'Target', image_label

    print 'Training Data distribution'
    print train_stat
    print 'Validation Data distribution'
    print valid_stat

    meta_info = {
        'train': {
            'total': sum(train_stat.values()),
            'classes': train_stat
        },
        'valid': {
            'total': sum(valid_stat.values()),
            'classes': valid_stat
        }
    }

    with open(os.path.join(DATA_DIR, 'metadata.json'), 'w') as outfile:
        json.dump(meta_info, outfile)

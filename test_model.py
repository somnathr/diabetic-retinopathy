# -*- coding: utf-8 -*-

from __future__ import print_function
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import os
import ast
import numpy as np
import itertools
from keras.preprocessing import image
from matplotlib import pyplot as plt
from glob import glob
from utils.model_definition import ret_model
from model_config import config as cf
from keras.optimizers import Adam
from sklearn.metrics import (accuracy_score, confusion_matrix,
                             classification_report, precision_recall_curve,
                             roc_curve, auc, average_precision_score)


def load_testset(dir_path):
    val_classes = os.listdir(dir_path)

    # store images
    X_test = []
    # store labels
    Y_test = []

    for val_class in val_classes:
        # get all images in the class
        list_images = glob(os.path.join(dir_path, val_class) + '/*.jpeg')

        # retirve images
        for image_path in list_images:
            img = image.img_to_array(
                image.load_img(
                    image_path, target_size=(cf['img_rows'], cf['img_cols'])))
            img /= 225.0
            X_test.append(img)
            Y_test.append(int(val_class))

    # create arrays
    X_test = np.stack(X_test)
    Y_test = np.stack(Y_test)

    print("Test set shape : ", X_test.shape)

    return X_test, Y_test


def load_model():
    # Load our model
    model = ret_model(
        cf['img_rows'],
        cf['img_cols'],
        cf['channel'],
        cf['num_classes'],
        optimizer=Adam(cf['learning_rate']),
        loss='categorical_crossentropy',
        weights_path='./models/weights.hdf5')

    return model


def accuracy(Y_true, Y_pred):
    score = accuracy_score(Y_true, Y_pred)
    print("Model Accuracy :", score)


def confusion_mat(Y_true, Y_pred, classes):
    cm = confusion_matrix(Y_true, Y_pred)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    print("Normalized confusion matrix")
    print(cm)

    plt.clf()
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title('Diabetic Retinopathy')
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes.values(), rotation=45)
    plt.yticks(tick_marks, classes.values())

    fmt = '.2f'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(
            j,
            i,
            format(cm[i, j], fmt),
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    plt.savefig('conf_mat.png')


def model_report(Y_true, Y_pred):
    print('---Classification Report---')
    print(classification_report(Y_true, Y_pred))


def plot_pr_curve(Y_true, Y_pred):
    precision, recall, _ = precision_recall_curve(Y_true, Y_pred)
    average_precision = average_precision_score(Y_true, Y_pred)

    plt.clf()
    plt.step(recall, precision, color='b', alpha=0.2, where='post')
    plt.fill_between(recall, precision, step='post', alpha=0.2, color='b')

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.title('2-class Precision-Recall curve: AP={0:0.2f}'.format(
        average_precision))

    plt.savefig('pr_curves.png')


def plot_roc_curve(Y_true, Y_pred):

    tpr, fpr, _ = roc_curve(Y_true, Y_pred)
    roc_auc = auc(fpr, tpr)
    print('AUC = %0.2f' % roc_auc)

    plt.clf()
    plt.xlim([-0.05, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.title('ROC Curve')

    plt.plot(
        tpr, fpr, 'b', color='darkorange', label='AUC = %0.2f' % 0.88756, lw=2)
    plt.legend(loc='lower right')

    plt.savefig('roc_curves.png')


def confusion_matrix_qwk(rater_a, rater_b, min_rating=None, max_rating=None):
    """
    Returns the confusion matrix between rater's ratings
    """
    assert (len(rater_a) == len(rater_b))
    if min_rating is None:
        min_rating = min(rater_a + rater_b)
    if max_rating is None:
        max_rating = max(rater_a + rater_b)
    num_ratings = int(max_rating - min_rating + 1)
    conf_mat = [[0 for i in range(num_ratings)] for j in range(num_ratings)]
    for a, b in zip(rater_a, rater_b):
        conf_mat[a - min_rating][b - min_rating] += 1
    return conf_mat


def histogram(ratings, min_rating=None, max_rating=None):
    """
    Returns the counts of each type of rating that a rater made
    """
    if min_rating is None:
        min_rating = min(ratings)
    if max_rating is None:
        max_rating = max(ratings)
    num_ratings = int(max_rating - min_rating + 1)
    hist_ratings = [0 for x in range(num_ratings)]
    for r in ratings:
        hist_ratings[r - min_rating] += 1
    return hist_ratings


def quadratic_weighted_kappa_np(rater_a,
                                rater_b,
                                min_rating=None,
                                max_rating=None):
    """
    Calculates the quadratic weighted kappa
    quadratic_weighted_kappa calculates the quadratic weighted kappa
    value, which is a measure of inter-rater agreement between two raters
    that provide discrete numeric ratings.  Potential values range from -1
    (representing complete disagreement) to 1 (representing complete
    agreement).  A kappa value of 0 is expected if all agreement is due to
    chance.

    quadratic_weighted_kappa(rater_a, rater_b), where rater_a and rater_b
    each correspond to a list of integer ratings.  These lists must have the
    same length.

    The ratings should be integers, and it is assumed that they contain
    the complete range of possible ratings.

    quadratic_weighted_kappa(X, min_rating, max_rating), where min_rating
    is the minimum possible rating, and max_rating is the maximum possible
    rating
    """
    rater_a = np.array(rater_a, dtype=int)
    rater_b = np.array(rater_b, dtype=int)
    assert (len(rater_a) == len(rater_b))
    if min_rating is None:
        min_rating = min(min(rater_a), min(rater_b))
    if max_rating is None:
        max_rating = max(max(rater_a), max(rater_b))
    conf_mat = confusion_matrix_qwk(rater_a, rater_b, min_rating, max_rating)
    num_ratings = len(conf_mat)
    num_scored_items = float(len(rater_a))

    hist_rater_a = histogram(rater_a, min_rating, max_rating)
    hist_rater_b = histogram(rater_b, min_rating, max_rating)

    numerator = 0.0
    denominator = 0.0

    for i in range(num_ratings):
        for j in range(num_ratings):
            expected_count = (
                hist_rater_a[i] * hist_rater_b[j] / num_scored_items)
            d = pow(i - j, 2.0) / pow(num_ratings - 1, 2.0)
            numerator += d * conf_mat[i][j] / num_scored_items
            denominator += d * expected_count / num_scored_items

    print("Quad Weighted Kappa", 1.0 - numerator / denominator)


def model_tests(Y_true, Y_prob, classes):
    Y_pred = np.argmax(Y_prob, axis=1)

    accuracy(Y_true, Y_pred)
    confusion_mat(Y_true, Y_pred, classes)
    model_report(Y_true, Y_pred)
    quadratic_weighted_kappa_np(Y_true, Y_pred)
    plot_pr_curve(Y_true, Y_prob[:, 1])
    plot_roc_curve(Y_true, Y_prob[:, 1])


if __name__ == '__main__':

    model = load_model()
    X_test, Y_true = load_testset('data/validation')

    # load the dictionary that identifies each category to an index in the prediction vector
    with open('dr2_clsid_to_human.txt') as classes_file:
        classes_dict = ast.literal_eval(classes_file.read())

    print('Evaluating %d data points' % (Y_true.shape[0]))

    Y_pred = model.predict(X_test, batch_size=32, verbose=1)

    model_tests(Y_true, Y_pred, classes_dict)

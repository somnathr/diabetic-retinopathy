# -*- coding: utf-8 -*-

from utils.model_definition import ret_model
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping, CSVLogger
from keras.optimizers import Adam
from model_config import config as cf
import time
import numpy as np

if __name__ == '__main__':

    np.random.seed(1)

    # prepare data augmentation configuration
    # rescaling, zooming and horizontal flipping the image provides
    # catergorizable images
    train_datagen = ImageDataGenerator(rescale=1. / 255)

    valid_datagen = ImageDataGenerator(rescale=1. / 255)

    train_generator = train_datagen.flow_from_directory(
        cf['train_data_dir'],
        target_size=(cf['img_rows'], cf['img_cols']),
        batch_size=cf['batch_size'])

    valid_generator = valid_datagen.flow_from_directory(
        cf['valid_data_dir'],
        target_size=(cf['img_rows'], cf['img_cols']),
        batch_size=cf['batch_size'])

    # Load our model
    model = ret_model(
        cf['img_rows'],
        cf['img_cols'],
        cf['channel'],
        cf['num_classes'],
        optimizer=Adam(cf['learning_rate']),
        loss='categorical_crossentropy',
        weights_path='./models/weights.hdf5')

    # setup tensorboard
    tensorboard = TensorBoard(
        log_dir='./logs/{}'.format(time.time()),
        batch_size=32,
        write_graph=True,
        write_images=True)

    # setup early stopping
    stop = EarlyStopping(
        monitor='val_acc', min_delta=0.001, patience=2, verbose=1, mode='auto')

    # setup model save callback
    weight_save_callback = ModelCheckpoint(
        './models/weights.hdf5',
        verbose=1,
        monitor='val_acc',
        save_best_only=True,
        save_weights_only=True,
        mode='auto',
        period=1)

    # setup csv logger
    csv_logger = CSVLogger('models/training_log.csv')

    # Start Fine-tuning
    model.fit_generator(
        train_generator,
        steps_per_epoch=cf['nb_train_samples'] // cf['batch_size'],
        epochs=cf['nb_epoch'],
        verbose=1,
        class_weight=cf['class_weights'],
        validation_data=valid_generator,
        validation_steps=cf['nb_valid_samples'] // cf['batch_size'],
        callbacks=[tensorboard, weight_save_callback, stop, csv_logger])

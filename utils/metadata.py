# -*- coding: utf-8 -*-

import json


def get_metadata():
    try:
        with open('./data/metadata.json', 'r') as infofile:
            metadata = json.load(infofile)
            return metadata
    except IOError:
        print 'Metadata file not found. Run prepare_data.py'

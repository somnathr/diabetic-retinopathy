# -*- coding: utf-8 -*-

from keras.optimizers import SGD
from keras.layers import (Input, Dense, MaxPooling2D, Dropout, ZeroPadding2D,
                          Activation, SeparableConv2D, GlobalAveragePooling2D)
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras import backend as K


def ret_model(img_rows,
              img_cols,
              color_type=1,
              num_classes=1000,
              optimizer=None,
              loss=None,
              weights_path=None):
    """
    Parameters:
      img_rows, img_cols - resolution of inputs
      channel - 1 for grayscale, 3 for color
      num_classes - number of class labels for our classification task
    """

    # Handle Dimension Ordering for different backends
    global bn_axis
    if K.image_dim_ordering() == 'tf':
        bn_axis = 3
        img_input = Input(shape=(img_rows, img_cols, color_type))
    else:
        bn_axis = 1
        img_input = Input(shape=(color_type, img_rows, img_cols))

    x = SeparableConv2D(
        64, (7, 7), strides=(2, 2), padding='same', name='conv1')(img_input)
    x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2))(x)

    x = SeparableConv2D(
        filters=128, kernel_size=(3, 3), padding='same', name='conv2a')(x)
    x = BatchNormalization(axis=bn_axis, name='bn_conv2a')(x)
    x = Activation('relu')(x)
    x = SeparableConv2D(
        filters=128, kernel_size=(5, 5), padding='same', name='conv2b')(x)
    x = BatchNormalization(axis=bn_axis, name='bn_conv2b')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2))(x)
    x = Dropout(0.5)(x)

    x = SeparableConv2D(
        filters=256, kernel_size=(3, 3), padding='same', name='conv3a')(x)
    x = BatchNormalization(axis=bn_axis, name='bn_conv3a')(x)
    x = Activation('relu')(x)
    x = SeparableConv2D(
        filters=256, kernel_size=(5, 5), padding='same', name='conv3b')(x)
    x = BatchNormalization(axis=bn_axis, name='bn_conv3b')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2))(x)

    x = SeparableConv2D(
        filters=512, kernel_size=(3, 3), padding='same', name='conv4a')(x)
    x = BatchNormalization(axis=bn_axis, name='bn_conv4a')(x)
    x = Activation('relu')(x)
    x = SeparableConv2D(
        filters=512, kernel_size=(5, 5), padding='same', name='conv4b')(x)
    x = BatchNormalization(axis=bn_axis, name='bn_conv4b')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2))(x)
    x = Dropout(0.5)(x)

    x = SeparableConv2D(
        filters=1024, kernel_size=(3, 3), padding='same', name='conv5a')(x)
    x = BatchNormalization(axis=bn_axis, name='bn_conv5a')(x)
    x = Activation('relu')(x)
    x = SeparableConv2D(
        filters=1024, kernel_size=(5, 5), padding='same', name='conv5b')(x)
    x = BatchNormalization(axis=bn_axis, name='bn_conv5b')(x)
    x = Activation('relu')(x)
    x_fc = GlobalAveragePooling2D()(x)

    x_fc = Dense(
        num_classes, activation='softmax', name='fc' + str(num_classes))(x_fc)

    # Create model
    model = Model(img_input, x_fc)

    if weights_path is not None:
        model.load_weights(weights_path)

    print model.summary()

    # Learning rate is changed to 0.001
    if optimizer is None:
        optimizer = SGD(lr=1e-4, decay=1e-6, momentum=0.9, nesterov=True)
    if loss is None:
        loss = 'categorical_crossentropy'
    model.compile(optimizer=optimizer, loss=loss, metrics=['accuracy'])

    return model
